#include <QtWidgets>

#include "mainwindow.h"

MainWindow::MainWindow() {
	QWidget *widget = new QWidget;
	setCentralWidget(widget);

	layout = new QVBoxLayout;
	layout->setContentsMargins(8,8,8,8);
	layout->setAlignment(Qt::AlignTop);
	widget->setLayout(layout);

	this->setStyleSheet("QMainWindow{background-color: rgb(230, 230, 230)}");

	colorPickerDialog = new QColorDialog;

	buildUI();
	buildMenu();

	setWindowTitle("untitled — Qt Paint Tool");
	setMinimumSize(450, 390);
	resize(800, 500);
}

void MainWindow::buildMenu() {
	aNew = new QAction("New...", this);
	aOpen = new QAction("Open...", this);
	aSave = new QAction("Save", this);
	aSaveAs = new QAction("Save As...", this);
	aCloseFile = new QAction("Close", this);
	aExit = new QAction("Exit", this);
	aAbout = new QAction("About", this);

	connect(aNew, &QAction::triggered, this, &MainWindow::newFile);
	connect(aOpen, &QAction::triggered, this, &MainWindow::open);
	connect(aSave, &QAction::triggered, this, &MainWindow::save);
	connect(aSaveAs, &QAction::triggered, this, &MainWindow::saveAs);
	connect(aCloseFile, &QAction::triggered, this, &MainWindow::closeFileWithDialog);
	connect(aExit, &QAction::triggered, this, &MainWindow::close);
	connect(aAbout, &QAction::triggered, this, &MainWindow::about);

	menuFile = menuBar()->addMenu("File");
	menuFile->addAction(aNew);
	menuFile->addAction(aOpen);
	menuFile->addAction(aSave);
	menuFile->addAction(aSaveAs);
	menuFile->addAction(aCloseFile);
	menuFile->addSeparator();
	menuFile->addAction(aExit);

	menuBar()->addSeparator();

	menuHelp = menuBar()->addMenu("Help");
	menuHelp->addAction(aAbout);
}

void MainWindow::buildUI() {
	QHBoxLayout *hbox = new QHBoxLayout;
	hbox->setAlignment(Qt::AlignLeft);

	QHBoxLayout *statusBar = new QHBoxLayout;
	statusBar->setAlignment(Qt::AlignLeft);
	statusBar->addWidget(new QLabel("Current color:"));

	mainPicker = new ColorPicker(20);
	mainPicker->setColor(QColor(0,0,0));
	connect(mainPicker, &ColorPicker::clicked, this, &MainWindow::onColorPickerClick);
	statusBar->addWidget(mainPicker);

	QVBoxLayout *sidebar = new QVBoxLayout;
	sidebar->setAlignment(Qt::AlignTop);

	const int COUNT = 8;
	ColorPicker *colorPickers[COUNT];
	QColor colors[COUNT] = {QColor(255,0,0), QColor(255,127,0), QColor(255,255,0),
								QColor(0,255,0), QColor(0,0,255), QColor(75,0,130),
								QColor(143,0,255), QColor(0,0,0)};
	for (int i = 0; i < COUNT; i++) {
		colorPickers[i] = new ColorPicker(35);
		colorPickers[i]->setColor(colors[i]);
		connect(colorPickers[i], &ColorPicker::clicked, this, &MainWindow::onColorPaletteItemClick);
		sidebar->addWidget(colorPickers[i]);
	}


	QVBoxLayout *sheet = new QVBoxLayout();
	sheet->setAlignment(Qt::AlignTop);

	shapeSelector = new QComboBox();
	shapeSelector->addItem("● Dot");
	shapeSelector->addItem("‒ Line");
	shapeSelector->addItem("▭ Rect");
	shapeSelector->addItem("○ Ellipse");

	brushWidthSelector = new QSpinBox();
	brushWidthSelector->setValue(8);
	brushWidthSelector->setAlignment(Qt::AlignCenter);
	brushWidthSelector->setMinimum(1);
	brushWidthSelector->setMaximum(200);

	renderArea = new RenderArea(mainPicker->getColor(),
								brushWidthSelector->value(),
								Shape(shapeSelector->currentIndex()),
								600, 400);
	scrollArea = new QScrollArea;
	scrollArea->setBackgroundRole(QPalette::Dark);
	scrollArea->setWidget(renderArea);

	sheet->addWidget(scrollArea);

	statusBar->addWidget(shapeSelector);
	statusBar->addWidget(new QLabel("Brush width:"));
	statusBar->addWidget(brushWidthSelector);
	statusBar->addWidget(new QSplitter());

	connect(shapeSelector, SIGNAL(currentIndexChanged(int)), this, SLOT(onShapeSelected(int)));
	connect(brushWidthSelector, SIGNAL(valueChanged(int)), this, SLOT(onBrushWidthChanged(int)));

	hbox->addLayout(sidebar);
	hbox->addLayout(sheet);
	this->layout->addLayout(statusBar);
	this->layout->addLayout(hbox);
}

void MainWindow::onColorPickerClick() {
	const QColor color = colorPickerDialog->getColor(colorPickerDialog->currentColor(), this);
	if (color.isValid()) {
		colorPickerDialog->setCurrentColor(color);
		mainPicker->setColor(color);
		if (renderArea != nullptr)
			renderArea->currentColor = color;
	}
}

void MainWindow::onColorPaletteItemClick() {
	QObject* obj = sender();
	ColorPicker *colorPicker = qobject_cast<ColorPicker *>(obj);
	QColor color = colorPicker->getColor();
	mainPicker->setColor(color);
	if (renderArea != nullptr) {
		renderArea->currentColor = color;
	}
}

void MainWindow::onShapeSelected(int index) {
	if (renderArea != nullptr) {
		renderArea->currentShape = Shape(index);
	}
}

void MainWindow::onBrushWidthChanged(int value) {
	if (renderArea != nullptr) {
		renderArea->currentBrushWidth = value;
	}
}

void MainWindow::newFile() {
	if (maybeSave()) {
		this->closeFile();

		QDialog dialog(this);
		QFormLayout form(&dialog);
		form.addRow(new QLabel("Please enter the resolution"));

		QSpinBox *lWidth = new QSpinBox(&dialog);
		lWidth->setAlignment(Qt::AlignCenter);
		lWidth->setMinimum(1);
		lWidth->setMaximum(std::numeric_limits<int>::max());
		lWidth->setValue(600);
		QSpinBox *lHeight = new QSpinBox(&dialog);
		lHeight->setAlignment(Qt::AlignCenter);
		lHeight->setMinimum(1);
		lHeight->setMaximum(std::numeric_limits<int>::max());
		lHeight->setValue(400);

		form.addRow(new QLabel("Width:"), lWidth);
		form.addRow(new QLabel("Height:"), lHeight);

		QDialogButtonBox buttonBox(QDialogButtonBox::Ok, Qt::Horizontal, &dialog);
		form.addRow(&buttonBox);
		QObject::connect(&buttonBox, SIGNAL(accepted()), &dialog, SLOT(accept()));

		dialog.setFixedSize(QSize(225, 140));
		if (dialog.exec() == QDialog::Accepted) {
			renderArea = new RenderArea(mainPicker->getColor(),
										brushWidthSelector->value(),
										Shape(shapeSelector->currentIndex()),
										lWidth->value(), lHeight->value());
			scrollArea->setWidget(renderArea);

			currentFile = QString();
			setWindowTitle("untitled — Qt Paint Tool");
			aSave->setEnabled(true);
			aSaveAs->setEnabled(true);
			aCloseFile->setEnabled(true);
		}
	}
}

void MainWindow::open() {
	if (maybeSave()) {
		QString path = QFileDialog::getOpenFileName(this, "Open image", "", "Images (*.png *.jpg *.jpeg)");
		if (!path.isEmpty()) {
			QGuiApplication::setOverrideCursor(Qt::WaitCursor);
			renderArea = new RenderArea(mainPicker->getColor(),
										brushWidthSelector->value(),
										Shape(shapeSelector->currentIndex()),
										path);
			scrollArea->setWidget(renderArea);
			QGuiApplication::restoreOverrideCursor();
			currentFile = path;
			setWindowTitle(QFileInfo(path).fileName() + " — Qt Paint Tool");
			aSave->setEnabled(true);
			aSaveAs->setEnabled(true);
			aCloseFile->setEnabled(true);
		}
	}
}

void MainWindow::closeFileWithDialog() {
	if (maybeSave()) {
		this->closeFile();
	}
}

void MainWindow::closeFile() {
	if (renderArea != nullptr) {
		delete renderArea;
		renderArea = nullptr;
	}
	currentFile = QString();
	setWindowTitle("Qt Paint Tool");
	aSave->setEnabled(false);
	aSaveAs->setEnabled(false);
	aCloseFile->setEnabled(false);
}

bool MainWindow::save() {
	if (currentFile.isEmpty())
		return saveAs();
	saveFile(currentFile);
	return true;
}

bool MainWindow::saveAs() {
	QFileDialog dialog(this, "Save image as", "", "Images (*.png *.jpg *.jpeg)");
	dialog.setDefaultSuffix("png");
	dialog.setWindowModality(Qt::WindowModal);
	dialog.setAcceptMode(QFileDialog::AcceptSave);
	if (dialog.exec() != QDialog::Accepted)
		return false;
	saveFile(dialog.selectedFiles().at(0));
	return true;
}

bool MainWindow::maybeSave() {
	if ((renderArea != nullptr) && (renderArea->isModified)) {
		const QMessageBox::StandardButton ret
			= QMessageBox::warning(this, "Qt Paint Tool",
				"The document has been modified.\n"
				"Do you want to save your changes?",
				QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
		switch (ret) {
			case QMessageBox::Save:
				return save();
			case QMessageBox::Cancel:
				return false;
			default:
				return true;
		}
	}
	return true;
}

void MainWindow::saveFile(const QString &path) {
	QGuiApplication::setOverrideCursor(Qt::WaitCursor);
	renderArea->save(path);
	currentFile = path;
	setWindowTitle(QFileInfo(path).fileName() + " — Qt Paint Tool");
	QGuiApplication::restoreOverrideCursor();
}

void MainWindow::about() {
   QMessageBox::about(this, "About Qt Paint Tool",
	  "<b>Qt Paint Tool</b> allows you to draw vector images "
	  "with direct lines of different colors.<br><br>"
	  "Source code is available at:<br>"
	  "<a href='https://gitlab.com/SolinnenHub/Qt/qt-paint-tool'>https://gitlab.com/SolinnenHub/Qt/qt-paint-tool</a>");
}

void MainWindow::closeEvent(QCloseEvent *event) {
	if (maybeSave()) {
		event->accept();
	} else {
		event->ignore();
	}
}
