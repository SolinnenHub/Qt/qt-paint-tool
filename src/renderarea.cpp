#include "renderarea.h"

RenderArea::RenderArea(QColor c, int brushWidth, Shape s, QString _path)
	: currentColor(c), currentBrushWidth(brushWidth), currentShape(s) {
	this->path = _path;
	img = new QPixmap(this->path);
	this->init(img->width(), img->height());
}

RenderArea::RenderArea(QColor c, int brushWidth, Shape s, int w, int h)
	: currentColor(c), currentBrushWidth(brushWidth), currentShape(s) {
	img = new QPixmap(w, h);
	img->fill(Qt::white);
	this->init(w, h);
}

void RenderArea::init(int w, int h) {
	this->setCursor(Qt::CrossCursor);
	this->setMinimumSize(w, h);
	this->setMaximumSize(w, h);
}

void RenderArea::paintEvent(QPaintEvent *e) {
	QPainter painter(this);

	painter.drawPixmap(0, 0, *img);
	painter.setRenderHint(QPainter::Antialiasing);

	DrawCall *last = new DrawCall();
	for (auto const& call : *drawCalls) {
		if (call.shape == Shape::DOT) {
			if (last->shape != Shape::NONE) {
				draw(&painter, call.color, call.width, Shape::LINE, last->p1, call.p2);
			} else {
				draw(&painter, call.color, call.width, call.shape, call.p1, call.p2);
			}
		} else {
			draw(&painter, call.color, call.width, call.shape, call.p1, call.p2);
		}
		*last = call;
	}
	delete last;

	if (isDrawing) {
		draw(&painter, currentColor, currentBrushWidth, currentShape, drawBegin, drawEnd);
	}

	painter.setClipRect(0, 0, this->width(), this->height());
}

void RenderArea::draw(QPainter *painter, QColor color, int brushWidth, Shape shape, QPointF *p1, QPointF *p2) {
	painter->setPen(QPen(QBrush(color, Qt::BrushStyle(Qt::SolidPattern)),
				brushWidth, Qt::SolidLine, Qt::PenCapStyle(Qt::RoundCap),
				Qt::PenJoinStyle(Qt::SvgMiterJoin)));

	if(shape == Shape::NONE)
		return;

	QPointF _p2 = *p2 + QPointF(FLT_EPSILON, 0);
	switch (shape) {
	case Shape::DOT:
		return painter->drawLine(*p1, _p2);
	case  Shape::LINE:
		return painter->drawLine(*p1, _p2);
	case Shape::RECTANGLE:
		return painter->drawRect(QRectF(*p1, _p2));
	case Shape::ELLIPSE:
		return painter->drawEllipse(QRectF(*p1, _p2));
	default:
		return;
	}
}

void RenderArea::mousePressEvent(QMouseEvent *e) {
	isModified = true;
	QPointF *p = new QPointF(e->localPos().x(), e->localPos().y());
	if (currentShape != Shape::DOT) {
		isDrawing = true;
		drawBegin = p;
		drawEnd = p;
	} else {
		drawCalls->push_back(DrawCall(currentColor, currentBrushWidth, currentShape, p, p));
	}

	update();
}

void RenderArea::mouseMoveEvent(QMouseEvent *e) {
	QPointF *p = new QPointF(e->localPos().x(), e->localPos().y());
	if (currentShape == Shape::DOT) {
		drawCalls->push_back(DrawCall(currentColor, currentBrushWidth, currentShape, p, p));
	} else if (isDrawing) {
		drawEnd = p;
	}
	update();
}

void RenderArea::mouseReleaseEvent(QMouseEvent *e) {
	if (isDrawing) {
		QPointF *p2 = new QPointF(e->localPos().x(), e->localPos().y());
		drawCalls->push_back(DrawCall(currentColor, currentBrushWidth, currentShape, drawBegin, p2));
		isDrawing = false;
	}
	drawCalls->push_back(DrawCall());
	update();
}

void RenderArea::save(const QString &path) {
	QPixmap *pixmap = new QPixmap(this->size());
	this->render(pixmap);
	pixmap->save(path);
	this->isModified = false;
}
