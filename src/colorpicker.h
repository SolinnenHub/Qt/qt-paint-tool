#ifndef COLORPICKER_H
#define COLORPICKER_H

#include <QLabel>

class ColorPicker : public QWidget {
	Q_OBJECT

signals:
	void clicked();

public:
	ColorPicker(int size);

	void setColor(QColor color);
	QColor getColor();

private:
	QColor currentColor;
	void mousePressEvent(QMouseEvent* event);
};

#endif
