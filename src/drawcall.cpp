#include "drawcall.h"

DrawCall::DrawCall(QColor c, int w, Shape s, QPointF *_p1, QPointF *_p2)
	: color(c), width(w), shape(s), p1(_p1), p2(_p2) {}

DrawCall::DrawCall() {
	this->shape = Shape::NONE;
}
