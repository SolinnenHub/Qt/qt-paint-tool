#ifndef RENDERAREA_H
#define RENDERAREA_H

#include <QPixmap>
#include <QWidget>
#include <QPainter>
#include <QMouseEvent>

#include <iostream>
#include <cfloat>

#include "drawcall.h"

class RenderArea : public QWidget {
	Q_OBJECT

public:
	RenderArea(QColor color, int currentBrushWidth, Shape currentShape, QString path);
	RenderArea(QColor color, int currentBrushWidth, Shape currentShape, int w, int h);
	void save(const QString &path);
	bool isModified = false;

	QColor currentColor = Qt::black;
	int currentBrushWidth = 1;
    Shape currentShape = Shape::DOT;

private:
	void init(int w, int h);
	void paintEvent(QPaintEvent *e) override;
	void mousePressEvent(QMouseEvent *e) override;
	void mouseReleaseEvent(QMouseEvent *e) override;
	void mouseMoveEvent(QMouseEvent *e) override;
	void draw(QPainter *painter, QColor color, int brushWidth,
			  Shape shape, QPointF *p1, QPointF *p2);

	QString path = QString();
	QPixmap *img;

	bool isDrawing = false;
	std::list<DrawCall> *drawCalls = new std::list<DrawCall>;
	QPointF *drawBegin;
	QPointF *drawEnd;
};

#endif
