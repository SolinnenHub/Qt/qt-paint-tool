#ifndef DRAWCALL_H
#define DRAWCALL_H

#include <QColor>
#include <QPointF>

enum Shape {
    DOT,
	LINE,
    RECTANGLE,
	ELLIPSE,
	NONE
};

class DrawCall {
public:
	DrawCall(QColor color, int width, Shape shape, QPointF *p1, QPointF *p2);
	DrawCall();

	QColor color = Qt::black;
	int width = 1;
    Shape shape = Shape::DOT;
	QPointF *p1;
	QPointF *p2;
};

#endif
