#include "colorpicker.h"

ColorPicker::ColorPicker(int size) {
	this->setMaximumSize(size, size);
	this->setMinimumSize(size, size);

	this->setToolTip("Pick the color");
	this->setAutoFillBackground(true);
	this->setCursor(Qt::PointingHandCursor);
}

void ColorPicker::setColor(QColor color) {
	this->currentColor = color;
	this->setPalette(QPalette(color));
}

QColor ColorPicker::getColor() {
	return this->currentColor;
}

void ColorPicker::mousePressEvent(QMouseEvent* event) {
	emit clicked();
}
