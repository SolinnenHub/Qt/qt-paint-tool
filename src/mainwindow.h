#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QAction>
#include <QMenu>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QColorDialog>
#include <QColor>
#include <QScrollArea>
#include <QComboBox>
#include <QSpinBox>
#include <QSplitter>

#include "renderarea.h"
#include "colorpicker.h"

class MainWindow : public QMainWindow {
	Q_OBJECT

public:
	MainWindow();

public slots:
	void newFile();
	void open();
	bool save();
	bool saveAs();
	void closeFile();
	void closeFileWithDialog();
	void onColorPickerClick();
	void onColorPaletteItemClick();
	void onShapeSelected(int index);
	void onBrushWidthChanged(int value);
	void closeEvent(QCloseEvent *event) override;
	void about();

private:
	void buildMenu();
	void buildUI();
	void writeSettings();
	bool maybeSave();
	void saveFile(const QString &fileName);

	QVBoxLayout *layout;
	QScrollArea *scrollArea;
	static ColorPicker *colorPickers[];
	QComboBox *shapeSelector;
	QSpinBox *brushWidthSelector;
	ColorPicker *mainPicker;
	QColorDialog *colorPickerDialog;
	RenderArea *renderArea;
	QString currentFile;

	QMenu *menuFile;
	QMenu *menuHelp;

	QAction *aNew;
	QAction *aOpen;
	QAction *aSave;
	QAction *aSaveAs;
	QAction *aCloseFile;
	QAction *aExit;
	QAction *aAbout;
};

#endif
