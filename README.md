# Qt Paint Tool

A simple graphics editor written in C++.

![](./demo.png)

## How to build:

```bash
qmake
make
```

